'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/view1', {
        templateUrl: 'view1/view1.html',
        controller: 'View1Ctrl'
    });
}])

.controller('contactFormController', ['$scope', function($scope) {
    $scope.master = {};

    $scope.update = function(user) {

        $scope.master = angular.copy(user);
        alert("Wysłano");
    };

    $scope.reset = function() {
        $scope.user = angular.copy($scope.master);
    };

    $scope.reset();
}])

.directive('userInfo', function() {
    return {
        templateUrl: 'view1/templ1.html'
    };
})
.controller('View1Ctrl', [function() {

}]);