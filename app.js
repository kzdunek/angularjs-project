'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
        'ngRoute',
        'myApp.view0',
        'myApp.view1',
        'myApp.view2',
        'myApp.view3',
        'myApp.view4',
        'myApp.view5',
        'myApp.version'
    ]).
controller('MyController', ['$scope','notify', function ($scope, notify) {
    $scope.clickMenu = function(msg) {
        notify($scope.times);
        $scope.times = $scope.times + 1;
    };
    $scope.times = 0;
}]).
factory('notify', ['$window', function(win) {
    var iter = 0;
    return function(msg) {
        iter += 1;
        if(iter == 10) {
            alert("Koniecznie odwiedź stronę www.kwestiasmaku.com!");
            iter = 0;
        }
    };
}]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/view0'});
}]);

function myCtrl($scope) {
    //initiate an array to hold all active tabs
    $scope.activeTabs = [];

    //check if the tab is active
    $scope.isOpenTab = function (tab) {
        //check if this tab is already in the activeTabs array
        if ($scope.activeTabs.indexOf(tab) > -1) {
            //if so, return true
            return true;
        } else {
            //if not, return false
            return false;
        }
    }

    //function to 'open' a tab
    $scope.openTab = function (tab) {
        //check if tab is already open
        if ($scope.isOpenTab(tab)) {
            //if it is, remove it from the activeTabs array
            $scope.activeTabs.splice($scope.activeTabs.indexOf(tab), 1);
        } else {
            //if it's not, add it!
            $scope.activeTabs.push(tab);
        }
    }
}